# MegaCode

MegaCode es un sistema de enseñanza en programación. MegaCode utiliza la librería bloque para implementar los algoritmos de solución de problemas. También MegaCode utiliza OpenCV para la detección de rostros en la camara frontal así como utiliza TensorFlow para la detección de emociones con una base de datos propia. MegaCode utiliza firebase como base de datos en tiempo real para sincronizar marcadores y perfiles de estudiantes.

Proximamente se daran más detalles, la herramienta se encuentra actualmente en desarrollo.
