//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ApiRest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Niveles_Terminados
    {
        public long id { get; set; }
        public Nullable<long> UsuarioId { get; set; }
        public Nullable<int> NivelId { get; set; }
        public Nullable<bool> terminado { get; set; }
        public int puntaje { get; set; }
    
        public virtual Nivel Nivel { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
