//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ApiRest.Models
{
    using System;
    
    public partial class obtenerMarcadorUsuario_Result
    {
        public string nombre { get; set; }
        public short comandos { get; set; }
        public short si { get; set; }
        public short para { get; set; }
        public short mientras { get; set; }
        public Nullable<short> total { get; set; }
    }
}
