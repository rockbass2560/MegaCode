﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiRest.Models
{
    public class RegistroResponse
    {
        public long id { get; set; }
        public string token { get; set; }
    }
}